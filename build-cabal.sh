#!/usr/bin/env bash

set -e
out=_cabal_out
dir=libraries/Cabal/Cabal
if [[ -z "$GHC" ]]; then
  GHC="_build/stage1/bin/ghc"
fi
time $GHC -O -hidir $out -odir $out -i$dir $dir/Setup.hs +RTS -s -RTS $@
rm -R $out
